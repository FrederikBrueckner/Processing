/*
 * ’Pillemann’ by Frederik Brückner
 * this code snippet is released under MIT license
 * feel free to use it any way you want
*/

String word = "Pillemann";
char[] letters;
PFont font;
float angle;
float angle_speed;
color lightred = color(255,119,119);  
   
void setup() {
  size(500, 400);
     
  font = createFont("Arial", 60);
  textFont(font);
  letters = word.toCharArray();
}
   
void draw() {
   background(255,0);
   noStroke();
   fill(lightred);
   rect(0, 0, 500, 400, 40);
   fill(0);
     
   translate( (width-textWidth(word))/2, height/2+18);
      
   // calculate an angle between 0 and 15 degrees 
   // based on the mouse position on the y axis 
   float new_angle = map(mouseY, 0, height, -radians(15), radians(15));     
   angle_speed += (new_angle-angle) * 0.1;
   angle += angle_speed;
   angle_speed *= 0.9;
   
   for (int i=0; i < letters.length; i++) {
     rotate(angle);
     text(letters[i], 0, 0);     
     translate(textWidth(letters[i]), 0);
   }
}

