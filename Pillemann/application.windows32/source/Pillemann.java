import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Pillemann extends PApplet {

String word = "Pillemann";
char[] letters;
PFont font;
float angle;
float angle_speed;
int lightred = color(255,119,119);  
   
public void setup() {
  
     
  font = createFont("Arial", 60);
  textFont(font);
  letters = word.toCharArray();
}
   
public void draw() {
   background(255,0);
   noStroke();
   fill(lightred);
   rect(0, 0, 500, 400, 40);
   fill(0);
     
   translate( (width-textWidth(word))/2, height/2+18);
      
   // calculate an angle between 0 and 15 degrees 
   // based on the mouse position on the y axis 
   float new_angle = map(mouseY, 0, height, -radians(15), radians(15));     
   angle_speed += (new_angle-angle) * 0.1f;
   angle += angle_speed;
   angle_speed *= 0.9f;
   
   
   for (int i=0; i < letters.length; i++) {
     rotate(angle);
     text(letters[i], 0, 0);     
     translate(textWidth(letters[i]), 0);
     
   }
}
  public void settings() {  size(500, 400); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Pillemann" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
