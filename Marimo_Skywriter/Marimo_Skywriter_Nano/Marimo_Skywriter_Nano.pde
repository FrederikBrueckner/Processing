/*
 * Marimo for Pimoroni Skywriter on Arduino
 * by Frederik Brückner
 *
 * based on 'Esfera' by David Pena 
 *  
 */


import processing.serial.*;
PShader blur;

Serial myPort;  // create object from Serial class
String val = "00000:00000:00000";     // data from the serial port
int particles = 14000; // number of 'hair' instances
Hair[] list ; // array that holds the values for hair out of particles
float radius = 200; // main sphere diameter
float rxp = 0;
float ryp = 0;
float rzp = 1;
float rx = 0; // init mouseVarX
float ry = 0; // init mouseVarY
float rz = 0; // init mouseVarZ
float scalefactor; // init zoomFactor using scale()

void setup() {
  fullScreen(P3D); // enable fullScreen mode
  //size(800,600,P3D); // window mode
  surface.setResizable(true);
  blur = loadShader("blur.glsl");
  smooth(8); // enable 8x anti-aliasing
  scalefactor = 1; // init scalefactor to 1

  String portName = Serial.list()[0]; // change the 0 to a 1 or 2 etc. to match your port
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');

  radius = height/4.5; // set sphereRadius in relation to screenSize
  list = new Hair[particles]; // fill array with instances
  for (int i = 0; i < list.length; i++) {
    list[i] = new Hair();
  }
  noiseDetail(6);
}

void draw() { // main loop
  
  background(20,20);
  //println(frameRate); // framerate counter for performance check
  loadPixels(); // init pixel array //<>//
  
  //println(rxp, ryp, rzp); // uncomment to read scaled sensor input //<>// //<>//
  //rxp = (mouseX-(width/2)) * 0.01; // uncomment for mouse use
  //ryp = (mouseY-(height/2)) * -0.01;
   
  rx = rx * 0.8 + rxp * 0.2;
  ry = ry * 0.8 + ryp * 0.2;
  rz = rz * 0.8 + rzp * 0.2;
     
  translate(width/2, height/2); // keep object in the middle of  the screen
  rotateY(rx);
  rotateX(ry);
  scale(scalefactor); // zoom using scale funtion
  fill(0);
  noStroke();
  sphere(radius);

  for (int i = 0; i < list.length; i++) {
    list[i].draw();
  }
  filter(blur);
}

void serialEvent(Serial myPort){ // sensor input via serial port with exception handling
  try
  {
    while ( myPort.available() > 0) // do only when port is available
    {  
      if(val != null) // check for NullPointerException
        {
          val = myPort.readStringUntil('\n');         // read data and store string in val
          float[] coords = float(split(val, ':')); // split datapackage and convert to float
          rxp = ((((coords[0])/65535)*width)-(width/2)) * 0.00314; // scale received data
          ryp = ((((coords[1])/65535)*width)-(width/2)) * 0.00314;
          rzp = ((coords[2])/65535);
          scalefactor = 1.5-rz;
        }
     } 
   }
   catch (Exception e) { // exception handling
     println(e);
   }
 }

void mouseWheel(MouseEvent event) //funtion to enable mouseWheel zoom
{
  float e = event.getCount(); //note: MouseEvent methode getAmount() is deprecated, use getCount() instead
  scalefactor += e *.05; //increment zoomFactor accordingly
}

class Hair // class to generate random 'hair' values
{
  float z = random(-radius, radius);
  float phi = random(TWO_PI);
  float length = random(1.2, 1.3);
  float theta = asin(z/radius);
  int colorshade = 0x2010D040 + int(random(50));
  
  Hair() { // what's wrong with a constructor here
    z = random(-radius, radius);
    phi = random(TWO_PI);
    length = random(1.2, 1.3);
    theta = asin(z/radius);
  }

  void draw() { // animate and draw lines from instance
     
    float off = (noise(millis() * 0.0004, sin(phi))-0.5) * 0.3;
    float offb = (noise(millis() * 0.0004, sin(z) * 0.01)-0.4) * -0.3;

    float thetaff = theta+off;
    float phff = phi+offb;
    float x = radius * cos(theta) * cos(phi);
    float y = radius * cos(theta) * sin(phi);
    float z = radius * sin(theta);

    float xo = radius * cos(thetaff) * cos(phff);
    float yo = radius * cos(thetaff) * sin(phff);
    float zo = radius * sin(thetaff);

    float xb = xo * length;
    float yb = yo * length;
    float zb = zo * length;

    strokeWeight(2); // base thickness
    beginShape(LINES);
    stroke(0xDD010B0E);
    vertex(x, y, z);
    //strokeCap(ROUND);
    strokeWeight(0.1); // top thickness
    stroke(colorshade);
    vertex(xb, yb, zb);
    endShape();
  }
}