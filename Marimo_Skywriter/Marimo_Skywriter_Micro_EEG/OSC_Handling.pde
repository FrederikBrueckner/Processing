//EEG Vars
 

OscP5 oscP5;
NetAddressList myNetAddressList = new NetAddressList();
int myListeningPort = 5001;
int myBroadcastPort = 12000;
boolean doMuse = true;
boolean ready = false;
String myConnectPattern = "/eeg/connect";
String myDisconnectPattern = "/eeg/disconnect";
PFont myFont;

// muse values
int eeg_output_frequency_hz = 0;
int notch_frequency_hz = 0;
int battery_percent_remaining = 0;
int status_indicator[];
int dropped_samples = 0;
float museEEG[];
float museAlphaAbs[];
float museBetaAbs[];
float museGammaAbs[];
float museDeltaAbs[];
float museThetaAbs[];
float museACC[];
float concentration;
float mellow;
float excitementL;
float excitementR;
float attention;
float valence_frontal;
float valence_temporal;
String[] museEEGband = { "TP1 Delta ( 1 -  4 Hz)", "TP1 Theta ( 5 -  8 Hz)", "TP1 Alpha ( 9 - 13 Hz)", "TP1 Beta  (13 - 30 Hz)", "TP1 Gamma (30 - 50 Hz)"};
String[] museOrientation = {"Vertikal", "Quer", "Längs"};


void oscEvent(OscMessage theOscMessage) {
//  println("broadcaster: oscEvent");
  if (ready) {

  /* check if the address pattern fits any of our patterns */
  if (theOscMessage.addrPattern().equals(myConnectPattern)) {
    connect(theOscMessage.netAddress().address());
  }
  else if (theOscMessage.addrPattern().equals(myDisconnectPattern)) {
    disconnect(theOscMessage.netAddress().address());
  }

  if(doMuse && theOscMessage.addrPattern().length()>4 && theOscMessage.addrPattern().substring(0,5).equals("/muse")) {
  
    if (theOscMessage.addrPattern().equals("/muse/config")) {
      String config_json = theOscMessage.get(0).stringValue();
      JSONObject jo = JSONObject.parse(config_json);
      //println("config: " + jo.getString("mac_addr"));
      eeg_output_frequency_hz = jo.getInt("eeg_output_frequency_hz");
      notch_frequency_hz = jo.getInt("notch_frequency_hz");
      battery_percent_remaining = jo.getInt("battery_percent_remaining");
      //println(theOscMessage.addrPattern() + ": " + config_json);
    }
    else if (theOscMessage.addrPattern().equals("/muse/annotation")) {
      println(theOscMessage.addrPattern());
      for (int i=0; i<5; i++) println(theOscMessage.get(i).stringValue());
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/horseshoe")) {
      for (int i=0; i<4; i++) status_indicator[i] = int(theOscMessage.get(i).floatValue());
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/eeg/dropped_samples")) {
      dropped_samples = theOscMessage.get(0).intValue();
    }
    else if (theOscMessage.addrPattern().equals("/muse/eeg")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }  
    else if (theOscMessage.addrPattern().equals("/muse/eeg/quantization")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/acc")) {
      for (int i=0; i<3; i++) museACC[i] = (theOscMessage.get(i).floatValue());
      oscP5.send(theOscMessage, myNetAddressList);
    }
     
    else if (theOscMessage.addrPattern().equals("/muse/elements/delta_absolute")) {
      for (int i=0; i<4; i++) museDeltaAbs[i] = (theOscMessage.get(i).floatValue());
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/theta_absolute")) {
      for (int i=0; i<4; i++) museThetaAbs[i] = (theOscMessage.get(i).floatValue());
      excitementL = (museBetaAbs[1] + museThetaAbs[1]) / (museBetaAbs[1] + museAlphaAbs[1]);
      excitementR = (museBetaAbs[2] - museThetaAbs[2]) / (museBetaAbs[2] + museThetaAbs[2]);
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/alpha_absolute")) {
      for (int i=0; i<4; i++) museAlphaAbs[i] = (theOscMessage.get(i).floatValue());
      valence_frontal = ((0.8 * valence_frontal) + (0.2 * ( museAlphaAbs[1] - museAlphaAbs[2])));
      valence_temporal = ((0.8 * valence_frontal) + (0.2 * ( museAlphaAbs[0] - museAlphaAbs[3])));
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/alpha_relative")) {
      //for (int i=0; i<4; i++) museAlphaAbs[i] = (theOscMessage.get(i).floatValue());
      //valence_alpha = museAlphaAbs[1] - museAlphaAbs[2];
      //valence_beta = museAlphaAbs[0] - museAlphaAbs[3];
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/beta_absolute")) {
      for (int i=0; i<4; i++) museBetaAbs[i] = (theOscMessage.get(i).floatValue());
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/gamma_absolute")) {
      for (int i=0; i<4; i++) museGammaAbs[i] = (theOscMessage.get(i).floatValue());
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/blink")) {
      oscP5.send(theOscMessage, myNetAddressList);
      int blinkVal = theOscMessage.get(0).intValue();
      println("muse blink "+blinkVal);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/jaw_clench")) {
      oscP5.send(theOscMessage, myNetAddressList);
      int jawClench = theOscMessage.get(0).intValue();
      println("Jaw clenched "+jawClench);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/raw_fft0")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/raw_fft1")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/raw_fft2")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/raw_fft3")) {
      oscP5.send(theOscMessage, myNetAddressList);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/experimental/concentration")) {
      oscP5.send(theOscMessage, myNetAddressList);
      concentration = theOscMessage.get(0).floatValue();
      println("Concentration "+concentration);
    }
    else if (theOscMessage.addrPattern().equals("/muse/elements/experimental/mellow")) {
      oscP5.send(theOscMessage, myNetAddressList);
      mellow = theOscMessage.get(0).floatValue();
      println("Mellow "+mellow);
      attention = excitementL - mellow;
    }
  }
  else {
    println(theOscMessage.addrPattern());
  }
  }
}


private void connect(String theIPaddress) {
  if (!myNetAddressList.contains(theIPaddress, myBroadcastPort)) {
     myNetAddressList.add(new NetAddress(theIPaddress, myBroadcastPort));
     println("### adding "+theIPaddress+" to the list.");
  } else {
     println("### "+theIPaddress+" is already connected.");
  }
  println("### currently there are "+myNetAddressList.list().size()+" remote locations connected.");
}



private void disconnect(String theIPaddress) {
  if (myNetAddressList.contains(theIPaddress, myBroadcastPort)) {
    myNetAddressList.remove(theIPaddress, myBroadcastPort);
    println("### removing "+theIPaddress+" from the list.");
  } else {
    println("### "+theIPaddress+" is not connected.");
  }
  println("### currently there are "+myNetAddressList.list().size());
}