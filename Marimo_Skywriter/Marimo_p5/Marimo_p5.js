/*
 * Marimo for Pimoroni Skywriter on Arduino
 * by Frederik Brückner
 *
 * based on 'Esfera' by David Pena 
 *  
 */

function setup() {
  var particles = 14000; // number of 'hair' instances
  var Hair = []; // array that holds the values for hair out of particles
  var radius = 200; // main sphere diameter
  var rxp = 0;
  var ryp = 0;
  var rzp = 1;
  var rx = 0; // init mouseVarX
  var ry = 0; // init mouseVarY
  var rz = 0; // init mouseVarZ
  var scalefactor; // init zoomFactor using scale()
  
  var canvas = createCanvas(400, 300, WEBGL); // create a canvas
  canvas.parent('sketch-holder');
  surface.setResizable(true);
  smooth(8); // enable 8x anti-aliasing
  scalefactor = 1; // init scalefactor to 1
    
  radius = height/4.5; // set sphereRadius in relation to screenSize
  var list = new Hair[particles]; // fill array with instances
  for (var i = 0; i < list.length; i++) {
    list[i] = new Hair();
  }
  noiseDetail(6);
}

function draw() {// main loop
  background(0xBB050D12);
  //println(frameRate); // framerate counter for performance check
  loadPixels(); // init pixel array //<>//
  //println(rxp, ryp, rzp); // uncomment to read scaled sensor input //<>// //<>//
  rxp = (mouseX-(width/2)) * 0.01; // uncomment for mouse use
  ryp = (mouseY-(height/2)) * -0.01;
  
  rx = rx * 0.8 + rxp * 0.2;
  ry = ry * 0.8 + ryp * 0.2;
  rz = rz * 0.8 + rzp * 0.2;
     
  translate(width/2, height/2); // keep object in the middle of  the screen
  rotateY(rx);
  rotateX(ry);
  scale(scalefactor); // zoom using scale funtion
  fill(0xEE010501);
  noStroke();
  //sphere(radius);
  
  for (var i = 0; i < list.length; i++) {
    list[i].draw();
  }
  
}

function mouseWheel(MouseEvent, event) //funtion to enable mouseWheel zoom
{
  var e = event.getCount(); //note: MouseEvent methode getAmount() is deprecated, use getCount() instead
  scalefactor += e *.05; //increment zoomFactor accordingly
}

function Hair() // generate random 'hair' values
{
  var z = random(-radius, radius);
  var phi = random(TWO_PI);
  var length = random(1.2, 1.3);
  var theta = asin(z/radius);
  var colorshade = 0x1040F050 + int(random(50));
  
    z = random(-radius, radius);
    phi = random(TWO_PI);
    length = random(1.2, 1.3);
    theta = asin(z/radius);
  
  function draw() { // animate and draw lines from instance
     
    var off = (noise(millis() * 0.0005, sin(phi))-0.5) * 0.3;
    var offb = (noise(millis() * 0.0005, sin(z) * 0.02)-0.4) * -0.3;

    var thetaff = theta+off;
    var phff = phi+offb;
    var x = radius * cos(theta) * cos(phi);
    var y = radius * cos(theta) * sin(phi);
    var z = radius * sin(theta);

    var xo = radius * cos(thetaff) * cos(phff);
    var yo = radius * cos(thetaff) * sin(phff);
    var zo = radius * sin(thetaff);

    var xb = xo * length;
    var yb = yo * length;
    var zb = zo * length;

    strokeWeight(2); // base thickness
    beginShape(LINES);
    stroke(0xBB062133);
    vertex(x, y, z);
    //strokeCap(ROUND);
    strokeWeight(0.1); // top thickness
    stroke(colorshade);
    vertex(xb, yb, zb);
    endShape();
  }
}
