/*
 * ’Word Snake’ by Frederik Brückner
 * this code snippet is released under MIT license
 * feel free to use it any way you want
*/

float[] x;
float[] y;
float segLength = 18;
String word="Head|body|tail~~~";
char[] letters;
PFont font;
int l;

void setup() {
  size(640, 360);
  font = createFont("Arial", 24, true);
  textFont(font);
  textAlign(CENTER);
  letters = word.toCharArray();
  l = letters.length;
  x = new float[l];
  y = new float[l];
}

void draw() {
  background(0);
  dragSegment(0, mouseX, mouseY);
  for(int i=0; i<x.length-1; i++) {
    dragSegment(i+1, x[i], y[i]);
  }
}

void dragSegment(int i, float xin, float yin) {
  float dx = xin - x[i];
  float dy = yin - y[i];
  float angle = atan2(dy, dx);  
  x[i] = xin - cos(angle) * segLength;
  y[i] = yin - sin(angle) * segLength;
  segment(x[i], y[i], angle, i);
}

void segment(float x, float y, float a, int l) {
  pushMatrix();
  translate(x, y);
  rotate(a);
  scale(-1, -1);
  text(letters[l], 0, 0);
  popMatrix();
}